<?php
ini_set("session.cookie_lifetime", 0);// для автоматичного розлогування при закритті браузера
session_start();

ini_set('display_errors', 1); // для вывода ошибок
error_reporting(E_ALL); // для вывода ошибок
//define('ROOT', dirname(__FILE__));// где dirname(__FILE__) - получение полного
// пути к файлу на диске (dirname - это функция
// _FILE_ - псевдоконстанта


define('ROOT', __DIR__);

//Подключает автозагрузчик классов

require_once '\application\autoload.php';
//Запускаем главную страницу
Application\Core\Route::start();
