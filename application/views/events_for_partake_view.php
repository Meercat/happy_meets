<section>
    <div class="container">
        <div class="row">
            <?php include_once 'moduls/left_sidebar.php'; ?>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Куди можна піти</h2>
                    <?php for ($i = 0; $i < count($data['events']); $i++): ?>
                        <?php include 'moduls/short_view_event.php'; ?>
                    <?php endfor; ?>

                </div><!--features_items-->
                <!-- Постраничная навигация  -->

                <?php echo $data['pagination']->get(); ?>

                <!-- Постраничная навигация  -->

            </div>
        </div>
    </div>
</section>