<!DOCTYPE html>
<html lang="en">
    <head>
        	
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Happy meets</title>
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css"> 

        <link href="/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/template/css/price-range.css" rel="stylesheet">
        <link href="/template/css/animate.css" rel="stylesheet">
        <link href="/template/css/main.css" rel="stylesheet">
        <link href="/template/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="/template/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/template/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/template/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/template/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/template/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +38 000 111 11 11</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i>alekseev.surik@gmail.com</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="logo pull-left">
                                <a href="/"><img img src="/template/images/home/logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <?php if(!isset ($_SESSION['user'])): ?>
                                        <li><a href="/user/login/"><i class="fa fa-lock"></i>Вхід</a></li>
                                        <li><a href="/user/register/"><i class="fa fa-lock"></i>Реєстрація</a></li>
                                        <?php else: ?>
                                        <li><a>Привіт, <?php echo $_SESSION['user'] ; ?></a>
                                        </li>
                                        <li><a href="/cabinet/"><i class="fa fa-user"></i>В кабінет</a>
                                        </li>
                                        <li><a href="/user/logout/"><i class="fa fa-unlock"></i>Вихід</a></li>
                                        <?php endif; ?>
                                         <li><a href="/contacts/"><i class="fa fa-envelope" aria-hidden="true"></i>Написати нам</a></li>
                                </ul>
                            </div>
                        </div>
                          <!--Search-->
                          <div class="col-sm-2 pull-right" id="search-box">
                              <form action="/search/" id="search-form" method="post">
                                  <input id="search-text" name="querySearch" placeholder="Поиск" type="text"/>
                                  <button id="search-button" type="submit">                     
                                      <span><i class="fa fa-search" aria-hidden="true"></i></span>
                                  </button>
                              </form>
                          </div>
                    <!--Search-->
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left"> 
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="/">Главная</a></li>
                                    
                                    <li class="dropdown">
                                        
                                        <a href="/category/ASC/">
                                            
                                            <i class="fa fa-angle-down"></i></a>
                                        <ul role="menu" class="sub-menu">
                                            
                                            <li><a href="/category/subcategory/ASC/">
                                                    
                                                </a>
                                            </li>
                                           
                                        </ul>
                                       
                                    </li> 
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 padding-right">
                        <div class="features_items"><!--features_items-->
                            <!--<h2 class="title text-center">Випадкові події</h2>-->
                            <?php include 'application/views/' . $content_view; ?>

                        </div><!--features_items-->

                    </div>
                </div>
            </div>
        </section>
				
		  <footer id="footer"><!--Footer-->
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <p class="pull-left">Copyright © 2018<?php if (date('Y') > 2018) echo " - ".date('Y'); ?></p>
                        <p class="pull-right">Developed by Oleksandr Alieksieiev</p>
                    </div>
                </div>
            </div>
</footer><!--/Footer-->

<script src="/template/js/jquery.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/price-range.js"></script>
<script src="/template/js/jquery.prettyPhoto.js"></script>
<script src="/template/js/main.js"></script>

    </body>
</html>