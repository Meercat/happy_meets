<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 padding-right">
                <div class="features_items">
                    <h2 class="title text-center">Зареєструватися</h2>

                    <div class="col-lg-4 col-sm-offset-4">
                        <div class="login-form">
                            <h2>Реєстрація</h2>
                            <form action="#" method="post" enctype="multipart/form-data">

                                <p>Ім'я</p>
                                <input type="text" name="user_name" required="">

                                <p>Пароль</p>
                                <input type="password" name="pass" required>

                                <p>Email</p>
                                <input type="email" name="email" required>

                                <p>Контактний телефон</p>
                                <input type="tel" name="phone" placeholder="" required>

                                <input type="submit" name="submit" class="btn btn-default" value="Зареєструватися">

                                <br/><br/>

                            </form>
                        </div>
                    </div>

                <!--</div>features_items-->
                <!-- Постраничная навигация  -->
                <?php // echo $pagination->get(); ?>
                <!-- Постраничная навигация  -->
            </div>
        </div>
    </div>
</section>

