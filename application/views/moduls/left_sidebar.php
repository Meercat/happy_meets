<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Керування</h2>
        <div class="panel-group category-products">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <br/><a href="/events/eventsIGo">Куди я вже йду?</a>
                    </h4><br/>
                    <h4 class="panel-title">
                        <a href="/events/allActualEvents">Куди можна піти?</a>
                    </h4><br/>
                    <h4 class="panel-title">
                        <a href="/events/create/">Створити свою подію</a>
                    </h4><br/>
                    <h4 class="panel-title">
                        <a href="/events/getCreatedByMeEvents">Переглянути створеній мною події</a>
                    </h4><br/>
                </div>
            </div>
        </div>
    </div>
</div>
