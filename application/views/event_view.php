<section>
    <div class="container">
        <div class="row">
            <?php include_once 'moduls/left_sidebar.php'; ?>

            <div class="col-sm-9 padding-right">
                    <div class="product-details"><!--product-details-->
                        <div class="row">
                            <h2 class="title text-center">Про подію</h2>
                            <div class="col-sm-5">
                                <!--                <div class="view-product">
                                                    <img src="<?php // echo Product::getImage($productId);  ?>" alt="" />
                                <?php //if (Product::getImage($product['id']) == "/upload/images/products/no-image.jpg"): ?>
                                                                        <img src="<?php //echo Product::getImage($product['id']);    ?>" alt="" />
                                <?php //else: ?>
                                                                        <img src="http://inetshop.zzz.com.ua/components/Stamp.php?id=<?php //echo $product['id'];    ?>" alt="" />
                                <?php //endif; ?>
                                                </div>-->
                            </div>
                            <div class="col-sm-7">
                                <div class="product-information"><!--/product-information-->
                                    <h2><?php echo $data['event_details']['name']; ?></h2>
                                    <p>Місто: <?php echo $data['event_details']['city']; ?></p>
                                    <br>
                                    <p>Адреса: <?php echo $data['event_details']['adress']; ?></p>
                                    <br>
                                    <p>Дата: <?php echo $data['event_details']['start_at']; ?></p>
                                    <br>
                                    <p>Час початку: <?php echo $data['event_details']['start_at']; ?></p>
                                    <br>
                                    <p>Загальна кількість місць: <?php echo $data['event_details']['place_quantity']; ?></p>
                                    <br>
                                    <p>Залишилось вільних місць: <?php echo $data['free_place']; ?></p>
                                    <br>

                                    <a href="/events/AddToEvent?event_id=<?php echo $data['event_details']['event_id']; ?>" class="btn btn-default add-to-visit"><i class="fa fa-user-plus"></i><br/>Я йду</a>

                                </div><!--/product-information-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h5>Про подію</h5>
                                <pre><?php echo $data['event_details']['description']; ?></pre>
                            </div>
                        </div>
                    </div><!--/product-details-->

            </div>
        </div>
    </div>
</section>