<section>
    <div class="container">
        <div class="row">
            <?php include_once 'moduls/left_sidebar.php'; ?>
            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Створити нову подію</h2>

                    <div class="col-lg-6">
                        <div class="login-form">

                            <?php if ($data) : ?>
                                <?php foreach ($data as $error): ?>
                                    <font color="red"><p><?php echo $error; ?></p></font>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <form action="#" method="post" enctype="multipart/form-data">

                                <p>Назва події</p>
                                <input type="text" name="name" required="">

                                <p>Опис події</p>
                                <input type="text" name="description" required>

                                <p>Назва міста</p>
                                <input type="text" name="city" required>

                                <p>Адреса(назва вулиці, номер будівлі, площа тощо)</p>
                                <input type="text" name="adress" placeholder="" required>

                                <p>Дата події та час її початку</p>
                                Рік: <input type="text" name="date" placeholder="РРРР-ММ-ДД" required>

                                Час:<input type="text" name="time" placeholder="гг:хх" required>

                                Максимальна кількість присутніх осіб:
                                <input type="number" name="quantity" pattern="[0-9]{,1000}" required>

                                <input type="submit" name="submit" class="btn btn-default" value="Створити">

                                <br/><br/>
                            </form>
                        </div>
                    </div>
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>

