<?php

namespace Application\Controllers;

use Application\Models\ModelUser;

class ControllerUser extends \Application\Core\Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function actionLogin()
    {

        $email    = '';
        $password = '';
        $userId   = false;
        $error    = '';

        if (isset($_POST['submit'])) {
            $email    = $_POST['email'];
            $password = $_POST['password'];

            // Проверяем существует ли пользователь
            $userId = new ModelUser();

            $userId = $userId->login($email, $password);

            if ($userId == false) {
                // Если данные неправильные - показываем ошибку
                $error = 'Неправильные данные для входа на сайт';
            } else {

                // Если данные правильные, запоминаем пользователя (сессия)
                $_SESSION['user'] = $userId;
                $auth             = $_SESSION['user'];

                //Перенаправляем пользователя в закрытую часть - кабинет
                header("Location: /cabinet/cabinetMain");
            }
        }

        $data = compact("userId", "auth", "error");
        $this->view->generate('login_view.php', 'template_view.php', $data);
    }

    function actionLogout()
    {

        unset($_SESSION['user']);
        header("Location: /");
    }

    function actionRegister()
    {

        if (isset($_POST['submit'])) {
            $user_name = $_POST['user_name'];
            $pass      = $_POST['pass'];
            $email     = $_POST['email'];
            $phone     = $_POST['phone'];

            $register = new ModelUser();

            $register = $register->register($user_name, $pass, $email, $phone);

            if ($register) {
                header("Location:  /cabinet/");
            }
        }

        $this->view->generate('register_view.php', 'template_view.php');
    }
}