<?php

namespace Application\Controllers;

class Controller404 extends \Application\Core\Controller
{

    function actionIndex()
    {
        $this->view->generate('404_view.php', 'template_view.php');
    }
}