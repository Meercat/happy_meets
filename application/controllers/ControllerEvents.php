<?php

namespace Application\Controllers;

use Application\Models\ModelEvents;
use Application\Core\Pagination;
use Application\Core\Model;

class ControllerEvents extends \Application\Core\Controller
{

    function __construct()
    {
        \Application\Core\Controller::isLogin();
        parent::__construct();
        
    }

    function actionIndex()
    {
        
    }

    //Акшн створити подію
    function actionCreate()
    {

        $errors = array();

        /* якщо відправлена нова подія на запис, то записуємо її в БД */

        if (isset($_POST['submit'])) {
            $name  = trim($_POST['name']);
            $description = trim($_POST['description']);
            $city        = trim($_POST['city']);
            $adress      = trim($_POST['adress']);
            $date       = trim($_POST['date']);
            $time       = trim($_POST['time']);
            $place_quantity = trim($_POST['quantity']);

            $start_at = $date . " " . $time . ":00";

            $new_event = new ModelEvents();

           //перевірка коректності введених даних
            $errors = $new_event->checkDataforEvent($name, $date, $time);

            if(count($errors) == 0)
            {
            //створюємо подію
            $add_event = $new_event->createEvent($name, $description,
                $city, $adress, $start_at, $place_quantity);

//           Якщо запис пройшов успішно, перенаправляємо користувача в кабінет
            if ($add_event) {
                    header("Location: /cabinet");
                    exit;
                }
            }
        }
        $this->view->generate('create_event_view.php', 'template_view.php', $errors);
    }

    //Акшн отримати(переглянути) всі створені мною події
    function actionGetCreatedByMeEvents()
    {

        $model_event = new ModelEvents();

        $events = $model_event->getCreatedByMeEvents($this->page);

        //Получаем общее коллисество созданых мной событий для пагинации
        $total = $model_event->getTotalNumEventsByUser();
        $total = (int) $total;

        $pagination = new Pagination($total, $this->page, Model::SHOW_BY_DEFAULT,
            'page');

        $view_data = compact('events', 'pagination', 'model_event');

        $this->view->generate('created_by_me_events_view.php',
            'template_view.php', $view_data);
    }

    //Акшн отримати(переглянути) всі події куди можна піти
    function actionAllActualEvents()
    {

        $model_event = new ModelEvents();
        $events      = $model_event->getAllActualEvents($this->page);

        //Получаем общее коллисество созданых мной событий для пагинации
        $total = $model_event->getTotalNumActualevents();
        $total = (int) $total;

        $pagination = new Pagination($total, $this->page, Model::SHOW_BY_DEFAULT,
            'page');

        $view_data = compact('events', 'pagination', 'model_event');

        $this->view->generate('events_for_partake_view.php',
            'template_view.php', $view_data);
    }

    //Акшн подивитися деталі події
    function actionEventDetails()
    {
        if (isset($_GET['event'])) {
            $event_id = $_GET['event'];
        }

        $model_event = new ModelEvents();

        $event_details = $model_event->getEventDetails($event_id);
        $free_place = $model_event->getEventFreePlace($event_id);

        $view_data = compact('event_details', 'model_event', 'free_place');

        $this->view->generate('event_view.php', 'template_view.php', $view_data);
    }

    //Акшн подивитися деталі події на яку йде юзер
    function actionEventDetailsIGo()
    {
        if (isset($_GET['event'])) {
            $event_id = $_GET['event'];
        }

        $model_event = new ModelEvents();

        $event_details = $model_event->getEventDetails($event_id);
        $free_place = $model_event->getEventFreePlace($event_id);

        $view_data = compact('event_details', 'model_event', 'free_place');

        $this->view->generate('event_details_I_go_view.php', 'template_view.php', $view_data);
    }

    //Акшн приєднатися до подіх
    function actionAddToEvent()
    {
        if (isset($_GET['event_id'])) {
            $event_id = $_GET['event_id'];
        }

        $model_event = new ModelEvents();

        $add_to_event = $model_event->addToEvent($event_id);

        $this->view->generate('add_or_not_add_to_event_view.php', 'template_view.php', $add_to_event);
    }

    //Акшн відмовитится від події на яку юзер записаний
    function actionRefuseEvent()
    {
        if (isset($_GET['event_id'])) {
            $event_id = $_GET['event_id'];
        }

        $model_event = new ModelEvents();

        $add_to_event = $model_event->RefuseEvent($event_id);

        if($add_to_event) {
            header('Location: /events/eventsIGo');
        }
    }

    //Акшн показує події на які йде юзер
    public function actionEventsIGo()
    {

        $model_event = new ModelEvents();

        $events = $model_event->getEventsIGo();

        $total = count($events);
        $total = (int) $total;

        $pagination = new Pagination($total, $this->page, Model::SHOW_BY_DEFAULT,
            'page');

        $view_data = compact('events', 'pagination', 'model_event');

        $this->view->generate('events_I_go_view.php', 'template_view.php',
            $view_data);
    }
}