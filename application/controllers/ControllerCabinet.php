<?php

namespace Application\Controllers;

use Application\Core\Controller;

class ControllerCabinet extends \Application\Core\Controller
{

    function __construct()
    {
        parent::__construct();
         $is_login = new Controller();
         $is_login = $is_login->isLogin();
    }

    function actionIndex()
    {

        /* Перевіряємо чи залогінений користувач
          якщо ні, то перенаправляємо на реєстрацію */

        $this->view->generate('cabinet_view.php', 'template_view.php');
    }

    function actionCabinetMain()
    {
        
        $this->view->generate('cabinet_view.php', 'template_view.php');
    }
}