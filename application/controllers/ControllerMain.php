<?php

namespace Application\Controllers;

use Application\Core\View;

class ControllerMain
{

    function __construct()
    {
        $this->view = new View();
    }



    function actionIndex()
    {
        $this->view->generate('main_view.php', 'template_view.php');
    }
}