<?php

namespace Application\Models;

class ModelUser extends \Application\Core\Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function login($email, $password)
    {

        $sql = 'SELECT * FROM users WHERE email = :email AND pass = :pass';

        $result = $this->db->prepare($sql);
        $result->bindParam(':email', $email, \PDO::PARAM_STR);
        $result->bindParam(':pass', $password, \PDO::PARAM_STR);
        $result->execute();

        $user = $result->fetch();
        if ($user) {
            return $user['user_id'];
        }

        return false;
    }

    public function auth($userId)
    {

        $_SESSION['user'] = $userId;
    }

    public function logout()
    {

        unset($_SESSION["user"]);
        return true;
    }

    public function register($user_name, $pass, $email, $phone)
    {

        $sql = 'INSERT INTO users (user_name, pass, email, phone) VALUE (:user_name, :pass, :email, :phone)';

        $result = $this->db->prepare($sql);
        $result->bindParam(':user_name', $user_name, \PDO::PARAM_STR);
        $result->bindParam(':pass', $pass, \PDO::PARAM_STR);
        $result->bindParam(':email', $email, \PDO::PARAM_STR);
        $result->bindParam(':phone', $phone, \PDO::PARAM_STR);
        $result->execute();

        $sql              = $this->db->query("SELECT LAST_INSERT_ID() FROM users");
        $last_id          = $sql->fetch(\PDO::FETCH_NUM);
        $_SESSION['user'] = $last_id[0];

        return $sql;
    }
}