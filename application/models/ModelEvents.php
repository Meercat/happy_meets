<?php

namespace Application\Models;

use Application\Core\Model;

class ModelEvents extends \Application\Core\Model
{

    function __construct()
    {
        parent::__construct();
        $this->carrent_date = date("Y-m-d");
    }

    public function createEvent($name, $description, $city, $adress,
                                $start_at, $place_quantity)
    {

        $user_id = $_SESSION['user'];
        
        $sql = 'INSERT INTO events (user_id, name, description, city, adress, start_at, place_quantity)'
            . ' VALUE(:user_id, :name, :description, :city, :adress, :start_at, :place_quantity)';

        $result = $this->db->prepare($sql);
        $result->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
        $result->bindParam(':name', $name, \PDO::PARAM_STR);
        $result->bindParam(':description', $description, \PDO::PARAM_STR);
        $result->bindParam(':city', $city, \PDO::PARAM_STR);
        $result->bindParam(':adress', $adress, \PDO::PARAM_STR);
        $result->bindParam(':start_at', $start_at, \PDO::PARAM_STR);
        $result->bindParam(':place_quantity', $place_quantity, \PDO::PARAM_INT);

        $result->execute();

        return $result;
    }

    public function getCreatedByMeEvents($page)
    {

        $user_id = $_SESSION['user'];
        $eventsArray = array();

        $page   = intval($page);
        $offset = ($page - 1) * Model::SHOW_BY_DEFAULT;
        $limit  = Model::SHOW_BY_DEFAULT;

        //Выбираем все созданные юзером события
        $query = $this->db->query("SELECT * FROM events"
            ." WHERE user_id=$user_id AND DATE(start_at)>='$this->carrent_date' ORDER BY start_at LIMIT $limit OFFSET $offset");

        $i = 0;
        foreach ($query as $row) {
            $eventsArray[$i] = $row;
            $i++;
        }

        return $eventsArray;
    }

    public function getTotalNumEventsByUser()
    {
        $carrent_date = date("Y-m-d");

        $user_id = $_SESSION['user'];
        $query     = $this->db->query("SELECT COUNT(*) FROM events WHERE user_id=$user_id AND DATE(start_at)>='$this->carrent_date' ");
        $count   = $query->fetchColumn();

        return $count;
    }

    public function getAllActualEvents($page)
    {
        $eventsArray = array();
        $page   = intval($page);
        $offset = ($page - 1) * Model::SHOW_BY_DEFAULT;
        $limit  = Model::SHOW_BY_DEFAULT;

        $carrent_date = date("Y-m-d");

        $query = $this->db->query("SELECT * FROM events  WHERE DATE(start_at)>='$this->carrent_date' ORDER BY start_at LIMIT $limit OFFSET $offset");
        $result = $query->fetchAll();

        return $result;
    }

    //отримуємо загальну кількість актуальних подій для пагінації
    public function getTotalNumActualevents()
    {
        $carrent_date = date("Y-m-d");
        
        $query = $this->db->query("SELECT * FROM events WHERE DATE(start_at)>='$this->carrent_date' ");
        $result = $query->fetchAll();

        return (count($result));
    }
    public function getEventDetails($event_id)

    {

        $query = $this->db->query("SELECT * FROM events WHERE event_id=$event_id");
        $result = $query->fetchAll();
        
        return $result[0];
    }

    public function addToEvent($event_id)
    {
        $user_id = $_SESSION['user'];

        //Перевіряємо, чи юзер вже зареєстрований на цю подію
        $query = $this->db->query("SELECT COUNT(*) FROM registrations WHERE event_id=$event_id && user_id=$user_id");
        $user_registred = $query->fetchAll();
        
        //Дізнаємося скільки є вільних місць на подію
      
        $free_place = $this->getEventFreePlace($event_id);

        //якщо юзер не записан на подію, і є вільні місця, записуємо юзера до події
        if (!$user_registred[0][0] && $free_place>0) {
            $sql    = "INSERT INTO registrations (user_id, event_id) VALUE(:user_id, :event_id)";
            $result = $this->db->prepare($sql);

            $result->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
            $result->bindParam(':event_id', $event_id, \PDO::PARAM_INT);

            $result->execute();

            return $result;
        }
        return false;
    }
    
    //Відмовитится від події на яку юзер записаний

    public function refuseEvent($event_id)
    {
        $user_id = $_SESSION['user'];

        $sql = "DELETE FROM registrations WHERE event_id=:event_id && user_id=:user_id";
        $result = $this->db->prepare($sql);
        
        $result->bindParam(':event_id', $event_id, \PDO::PARAM_INT);
        $result->bindParam(':user_id', $user_id, \PDO::PARAM_INT);
        $result->execute();

        return $result;
    }

    //Вибираємо всі події, на які йде юзер
    public function getEventsIGo()
    {
        $user_id = $_SESSION['user'];

        $query = $this->db->query("SELECT * FROM events LEFT JOIN registrations using(event_id)"
            . " WHERE DATE(start_at)>='$this->carrent_date' AND registrations.user_id=$user_id ORDER BY start_at");
        $result = $query->fetchAll();

        return $result;
    }

    public function getEventFreePlace($event_id)
    {
        $query = $this->db->query("SELECT place_quantity FROM events WHERE event_id=$event_id");
        $total_place = $query->fetchALL();

        $query = $this->db->query("SELECT COUNT(*) FROM registrations WHERE event_id=$event_id");
        $quantity_occupied_place = $query->fetchALL();

        $free_place = $total_place[0][0]-$quantity_occupied_place[0][0];

        return $free_place;
    }

    public function checkDataforEvent($name, $date, $time)
    {
        $errors = array();

        //перевіряємо довжину назви події
        if(strlen($name) < 3)
        {
            $errors[] = 'Довжина назви подіх не можу бути меньше 3-х символів!!!';
        }

        //перевіряємо формат введеня дати події
        $array_date = explode("-", $date);
        if(!checkdate($array_date[1], $array_date[2], $array_date[0]))
        {
            $errors[] = 'Дата події введена з помилкою!!!';
        }

        //перевіряємо дату на актуальність
        if ($array_date[0] < date("Y"))
        {
            $errors[] = 'Рік початку події не може бути раніше поточного!!!';
        }
        if($array_date[0] >= date("Y") && $array_date[1] < date("m"))
        {
            $errors[] = 'Місяць початку події не може бути раніше поточного!!!';
        }
        if($array_date[0] >= date("Y") && $array_date[1] >= date("m") && $array_date[2] < date("d"))
        {
            $errors[] = 'День початку події не може бути раніше поточного!!!';
        }

        //перевіряємо коректність формата введення часу початку події
        if(!preg_match('/[0-9]{2}:[0-9]{2}/', $time))
        {
            $errors[] = 'Час початку події введено в невірному форматі!!!';
        }

        //перевіряємо коректність часу початку події
        $array_time = explode(':', $time);
        if($array_time[0] > 24 || $array_time[1] > 60)
        {
            $errors[] = 'Час початку події введено з помилкою!!!';
        }

        return $errors;
    }
}