<?php
/*
  Класс-маршрутизатор для определения запрашиваемой страницы.
  > цепляет классы контроллеров и моделей;
  > создает экземпляры контролеров страниц и вызывает действия этих контроллеров.
 */

namespace Application\Core;

class Route
{

    static function start()
    {
        // контроллер и действие по умолчанию
        $controller_name = 'Main';
        $action_name     = 'Index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // получаем имя контроллера
        if (!empty($routes[1])) {
            $controller_name = ucwords($routes[1]);
        }

        // получаем имя экшена
        if (!empty($routes[2])) {
            $action = explode('?', $routes[2]);
            $action_name = ucwords($action[0]);
        }

        // подцепляем файл с классом контроллера
        $controller_file = 'Controller'.$controller_name.'.php';
        $controller_path = "application/controllers/".$controller_file;
        if (!file_exists($controller_path)) {
            /*
              правильно было бы кинуть здесь исключение,
              но для упрощения сразу сделаем редирект на страницу 404
             */
            Route::errorPage404();
        }

        // добавляем префиксы
        $controller_name = 'Controller'.$controller_name;
        $action_name     = 'action'.$action_name;

        // создаем контроллер
        $controller_name = 'Application\Controllers\\'.$controller_name;
        $controller      = new $controller_name;
        $action          = $action_name;



        if (method_exists($controller, $action)) {
            // вызываем действие контроллера
            $controller->$action();
        } else {
            // здесь также разумнее было бы кинуть исключение
            Route::errorPage404();
        }

        return $controller;
    }

     function errorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }
}