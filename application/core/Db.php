<?php

namespace Application\Core;

class Db
{

    public function getConnection()
    {
        $paramsPath = ROOT.'/application/config/db_params.php';
        $params     = include ($paramsPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db  = new \PDO($dsn, $params['user'], $params['password']);
        $db->exec("set names utf-8");
        return $db;
    }
}