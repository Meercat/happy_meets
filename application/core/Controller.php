<?php

namespace Application\Core;

use Application\Core\View;

class Controller
{
    public $model;
    public $view;
    protected $page = 1;

    function __construct()
    {
        $this->view = new View();

        // получаем имя Модели если она есть
        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])) {
            $name = 'Model'.ucwords($routes[1]);
            if (class_exists($name)) {
                var_dump($name);
                $this->model = new $name;
            }
        }
        //номер активнох сторінки в пагінаці
        if (isset($_GET['page'])) {
            $this->page = $_GET['page'];
        }
    }
    /* Перевіряємо чи залогінений користувач
      якщо ні, то перенаправляємо на реєстрацію */

    public function isLogin()
    {
        if (!isset($_SESSION['user'])) {
            header("Location: /user/register");
        }
    }

    //
    // действие (action), вызываемое по умолчанию
    function actionIndex()
    {
        // todo
    }
}