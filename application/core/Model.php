<?php

namespace Application\Core;

use \Application\Core\Db;

class Model
{
    protected $db;
    const SHOW_BY_DEFAULT = 6;

    /*
      Модель обычно включает методы выборки данных, это могут быть:
      > методы нативных библиотек pgsql или mysql;
      > методы библиотек, реализующих абстракицю данных. Например, методы библиотеки PEAR MDB2;
      > методы ORM;
      > методы для работы с NoSQL;
      > и др.
     */

    function __construct()
    {
        $this->db = new Db();
        $this->db = $this->db->getConnection();

    }

    // метод выборки данных
    public function get_data()
    {
        // todo
    }
}