<?php
//// подключаем файлы ядра
//require_once 'core/model.php';
//require_once 'core/view.php';
//require_once 'core/controller.php';
//
///*
//Здесь обычно подключаются дополнительные модули, реализующие различный функционал:
//	> аутентификацию
//	> кеширование
//	> работу с формами
//	> абстракции для доступа к данным
//	> ORM
//	> Unit тестирование
//	> Benchmarking
//	> Работу с изображениями
//	> Backup
//	> и др.
//*/
//
//require_once 'core/route.php';



function core($class) {

// project-specific namespace prefix
$prefix = 'Application\\Core\\';

// base directory for the namespace prefix
$base_dir = __DIR__ . '/Core/';

// does the class use the namespace prefix?
$len = strlen($prefix);
if (strncmp($prefix, $class, $len) !== 0) {
// no, move to the next registered autoloader
return;
}

// get the relative class name
$relative_class = substr($class, $len);

// replace the namespace prefix with the base directory, replace namespace
// separators with directory separators in the relative class name, append
// with .php
$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
// if the file exists, require it
if (file_exists($file)) {
require $file;
}
}

function controllers($class) {

// project-specific namespace prefix
$prefix = 'Application\\Controllers\\';

// base directory for the namespace prefix
$base_dir = __DIR__ . '/Controllers/';

// does the class use the namespace prefix?
$len = strlen($prefix);
if (strncmp($prefix, $class, $len) !== 0) {
// no, move to the next registered autoloader
return;
}

// get the relative class name
$relative_class = substr($class, $len);

// replace the namespace prefix with the base directory, replace namespace
// separators with directory separators in the relative class name, append
// with .php
$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
// if the file exists, require it
if (file_exists($file)) {
require $file;
}
}

function models($class) {

// project-specific namespace prefix
$prefix = 'Application\\Models\\';

// base directory for the namespace prefix
$base_dir = __DIR__ . '/Models/';

// does the class use the namespace prefix?
$len = strlen($prefix);
if (strncmp($prefix, $class, $len) !== 0) {
// no, move to the next registered autoloader
return;
}

// get the relative class name
$relative_class = substr($class, $len);

// replace the namespace prefix with the base directory, replace namespace
// separators with directory separators in the relative class name, append
// with .php
$file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
// if the file exists, require it
if (file_exists($file)) {
require $file;
}
}

spl_autoload_register('core');
spl_autoload_register('controllers');
spl_autoload_register('models');


//spl_autoload_register(function ($class_name)
//    {
//    var_dump($class_name);
//    $path = ROOT . $class_name . '.php';
//    if (is_file($path)) {
//            require_once $path;
//        }
//
//    $array_paths = array(
//        '/application/',
//        '/application/core/',
//        '/application/controllers/',
//        '/application/models/'
//    );
//
//    foreach ($array_paths as $path) {
//        $path = ROOT . $path . $class_name . '.php';
//        if (is_file($path)) {
//            require_once $path;
//        }
//    }

//    });